# python+requests+pytest+allure接口自动化测试框架

#### 介绍

**1.1 说明**

基于pytest框架编写的接口自动化测试框架，整个框架采用的是python+requests+pytest+allure的结构，也是当前使用最广泛的一种接口自动化框架，当然，这种组成结构网上很多，每个人的实现方式不同，我这里录制了一套视频教程，收费的，可以点击这个链接进去看课程大纲[https://m.tb.cn/h.5ffYDbT?tk=zs9rdA0OVIf](https://m.tb.cn/h.5ffYDbT?tk=zs9rdA0OVIf)

注意：目前git上的代码是不全的，关键的文件没放上来！！！

#### 软件架构

整体的框架结构如下图所示：

![img](images/jiegou.JPG)

#### 安装教程

1. python环境，建议最好安装python3.8以上的版本，因为很多第三方库已经不支持3.8以下
2. 首先安装本项目所需的依赖库，命令为：【pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/ -r F:\automaticAPI\pythonproject\requirements.txt】 ，F:\automaticAPI\pythonproject路径替换你自己的路径即可
3. java环境，因为生成的allure报告是基于java实现，所以需要java环境

#### 框架能够实现的功能

    1）接口信息采用yaml文件进行管理，一个yaml文件可以实现业务类型的接口和单接口多条测试用例;
    2）测试数据与测试用例分离，目前可以支持csv、Excel、xml等格式文件的数据读取;
    3）接口之间参数依赖，也就是说上下游之间的参数传递;
    4）接口断言方式，支持6种断言方式，包括字符串包含断言、相等断言、不相等断言、任意值断言以及数据库断言等;
    5）封装很多的工具类，如：数据库（MySQL、Oracle）、Redis（支持单节点、集群服务）的数据读取、kafka、mgonoDB等主流中间件的数据读存;
    6）消息推送，实现了邮箱推送、钉钉群消息推送、企业微信推送;
    7）测试用例文件通过工具生成;
    8）jenkins持续集成部署，有详细的部署视频教程和部署文档，傻瓜式操作跟着操作就一定能部署成功;
    9）不仅支持单接口的测试，更支持业务逻辑的接口测试。

#### 接口的管理

接口信息使用yaml格式去管理，这种格式的文件非常受开发者们喜欢，维护更加便捷，同样也非常适用于我们做自动化测试，一个yaml文件既可以使用单接口多用例，也可以实现业务场景，下面贴了一张yaml的接口信息：

![yaml](images/ap_info.png)

#### 测试报告展示

1. 这套框架集成了两种报告的展现形式，分别是内容丰富的allure报告和稍微简单点的tmreport报告

2. 首先来看看allure
   
   ![allure01](images/allure01.png)
   
   报告右侧的信息打印的很全
   
   ![](images/allure02.png)
   
   3.再来看看tmreport报告的形式
   
   ![](images/tm01.png)
   
   ![](images/tm02.png)
